$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "onlinegames_info_connect/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "onlinegames_info_connect"
  s.version     = OnlinegamesInfoConnect::VERSION
  s.authors     = ["Mike Petersen"]
  s.email       = ["mike@odania-it.de"]
  s.homepage    = "http://www.onlinegames-info.com"
  s.summary     = "Connector for Onlinegames-Info.com"
  s.description = "Connector for Onlinegames-Info.com"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0.0.rc1"

  s.add_development_dependency "sqlite3"
end

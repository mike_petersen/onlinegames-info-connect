class SessionController < ApplicationController
	before_action :require_no_user, :except => [:destroy]

	def index
		@user = User.new
	end

	def new
		redirect_to OnlinegamesInfoConnect::OgiOauth.instance.authorize(callback_url)
	end

	def callback
		@user_events = UserEvent.new

		if params[:error]
			error_msg = t('An error occurred')
			error_msg = t('You need to allow this application the login') if params[:error] == 'access_denied'

			flash[:error] = error_msg
			return redirect_to login_path
		end

		# Get token & data about player
		token = OnlinegamesInfoConnect::OgiOauth.instance.get_token params[:code], callback_url
		me = OnlinegamesInfoConnect::OgiOauth.instance.me(token)

		# Load user
		@user = User.where(ogi_uid: me['id']).first

		if not @user.nil? and @user.user_status != 'active'
			flash[:notice] = :account_not_active_ask_support.l
			return redirect_to login_url
		elsif @user.nil?
			@user = User.new(:ogi_uid => me['id'], :name => nil, :email => me['email'], :current_login_ip => request.remote_ip, :char_id => me['char_id'])

			# Set campaign and ref_id
			@user.campaign = cookies[:campaign]
			@user.ref_id = params[:ref_id] unless params[:ref_id].nil?
			@user.ref_id = cookies[:ref_id] unless cookies[:ref_id].nil?

			if @user.save!
				initialize_new_user(@user)
				flash[:notice] = t('Account created')
			else
				flash[:notice] = t('Account creation error')
				return redirect_to login_url
			end
		else
			if (@user.last_login_at.day + 1) == @user.current_login_at.day then
				@user.daily_login_count += 1

				# Experience Config
				bonuses = GameBase::get_daily_login_bonus

				# Select Bonus
				bonus = bonuses[@user.daily_login_count].nil? ? bonuses.last : bonuses[@user.daily_login_count]

				# Add Bonus
				GameBase::set_bonus(@user, bonus)

				@user_events.add_event({event_type: 'daily_login_bonus', days: @user.daily_login_count, dialog_id: 'dialog-daily_login_bonus', bonus: bonus, bonuses: bonuses})

				# Track daily login
				Resque.enqueue(TrackActionWorker, @user.id, 'daily_login')
			elsif @user.last_login_at.day != @user.current_login_at.day
				@user.daily_login_count = 0
			end

			# Set flash message
			flash[:notice] = t('Login successful!')

			# Check Vacation
			unless @user.vacation.nil?
				@user.vacation = nil
				flash[:notice] = t('Vacation mode deactivated!')
			end
		end

		# Set User Login
		@user.user_login(request.remote_ip)

		# Check User data
		check_user_data(@user)

		# User save
		@user.coins = me['coins'].to_i
		@user.oauth_token = token.token
		@user.save!

		# Save User ID
		session[:logged_in_user_id] = @user.id
		session[:user_id] = @user.id.to_s
		session[:logged_in_at] = Time.now
		session[:premium_time] = @user.premium_time
		session[:possible_user_names] = me['possible_user_names'] unless me['possible_user_names'].nil?

		redirect_to game_url
	end

	def destroy
		# Get redirect url
		result = OnlinegamesInfoConnect::OgiTokenApi.instance.logout_page(current_user, request.remote_ip)

		# Reset session
		reset_session
		flash[:notice] = t('Logout successful!')

		return redirect_to result['url'] if not result.nil? and not result['status'].nil? and "ok".eql?(result['status'])

		redirect_back_or_default login_url
	end

end

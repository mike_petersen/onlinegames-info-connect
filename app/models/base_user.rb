# -*- encoding : utf-8 -*-
class BaseUser
	include Mongoid::Document
	include Mongoid::Timestamps

	belongs_to :language

	field :name, :type => String
	field :ogi_uid, :type => String
	field :char_id, :type => String
	field :campaign, :type => String
	field :user_status, :type => String, :default => 'active'
	field :premium_time, :type => DateTime
	field :daily_login_count, :type => Integer
	field :refs, :type => Integer, :default => 0

	# Premium coins
	field :coins, :type => Integer, :default => 0

	# information
	field :current_login_at, :type => DateTime
	field :current_login_ip, :type => String
	field :last_login_at, :type => DateTime
	field :last_login_ip, :type => String
	field :vacation, :type => DateTime
	field :last_request_at, :type => DateTime
	field :login_count, :type => Integer, :default => 0
	field :daily_login_count, :type => Integer, :default => 0

	field :roles, :type => Array, :default => []

	index({ ogi_uid: 1 }, { unique: true })

	# Oauth
	field :oauth_token, :type => String
	index({ oauth_token: 1 }, { unique: true })

	attr_accessor :password, :password_repeat, :email

	has_many :user_roles, :foreign_key => :user_id

	# Validate name
	validates_length_of :name, :within => 3..30, :if => Proc.new {|u| not u.name.nil?}
	validates_format_of :name, :with => /\A[a-z\d_ -.öäüÖÄÜ]+\z/i, :if => Proc.new {|u| not u.name.nil?}

	def token
		return OAuth2::AccessToken.new(OnlinegamesInfoConnect::OgiOauth.instance.get_client, self.oauth_token)
	end

	attr_accessor :cached_user_roles
	def has_right(right)
		if self.cached_user_roles.nil?
			self.cached_user_roles = []
			self.user_roles.all.each do |role|
				self.cached_user_roles << role.role
			end
		end

		return true if self.cached_user_roles.include?(right)
		return true if self.cached_user_roles.include?('superadmin')
		return false
	end

	# Checks if Online in the last 15 minutes
	def get_last_action
		return (self.last_request_at + 15.minutes) > Time.now
	end

	def user_login(remote_ip)
		self.last_request_at = Time.now
		self.last_login_ip = self.current_login_ip
		self.last_login_at = self.current_login_at
		self.current_login_at = Time.now
		self.current_login_ip = remote_ip
		self.login_count += 1
		self.save!
	end

	before_create do
		self.current_login_at = Time.now
		self.last_login_at = Time.now
		self.premium_time = Time.now
		self.user_status = 'active'

		return true
	end
end

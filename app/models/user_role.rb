# -*- encoding : utf-8 -*-
class UserRole
	include Mongoid::Document

	belongs_to :user
end

class Language
	include Mongoid::Document
	#cache

	field :name, :type => String
	field :active, :type => Boolean, :default => false

	index({name: 1}, {unique: true})
end

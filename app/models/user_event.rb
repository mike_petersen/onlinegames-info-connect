class UserEvent
	@@events = []

	def self.add_event(data)
		@@events << data
	end

	def self.get_events()
		return @@events
	end

	def self.clear
		@@events = []
	end
end

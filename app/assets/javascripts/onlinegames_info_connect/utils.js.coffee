root = exports ? this

class root.Utils
	formatRestTime: (milliseconds) ->
		seconds = milliseconds / 1000

		# Calc minutes
		minutes = Math.floor(seconds / 60)
		seconds -= minutes * 60
		seconds = Math.floor(seconds)

		# Calc hours
		hours = Math.floor(minutes / 60)
		minutes -= hours * 60

		# Add zeros to seconds if needed
		seconds = "0#{seconds}" if seconds < 10
		minutes = "0#{minutes}" if minutes < 10

		"#{hours}:#{minutes}:#{seconds}" if hours > 0
		"#{minutes}:#{seconds}"

	formatNumber: (number) ->
		number += ''
		x = number.split('.')
		x1 = x[0]
		x2 = if x.length > 1 then ',' + x[1] else ''
		rgx = /(\d+)(\d{3})/

		while rgx.test(x1)
			x1 = x1.replace(rgx, '$1' + "." + "$2")

		x1 + x2

root.utils = new Utils()

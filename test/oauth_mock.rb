module OnlinegamesInfoConnect
	module OauthUsers
		# Create a mock response
		def me(token)
			{'id' => OnlinegamesInfoConnect::OgiOauth.instance.get_user_id_by_token(token), 'name' => Faker::Name.name, 'language' => 'de', 'email' => Faker::Internet.email}
		end

		def create_user(ip, login, email, password, campaign, language)
			{'status' => 'ok', 'url' => 'http://test.onlinegamesinfo.com/app_login?token=123123'}
		end

		def set_user_name(user, char_name)
			{'status' => 'ok'}
		end
	end

	module OauthPremium
		def add_free_coins(user, amount, reason)
			{'status' => 'ok'}
		end

		def balance(user)
			{'status' => 'ok', 'coins' => '300', 'last_payment' => {
				id: 1, channel_id: 1, user_id: user.id, character_id: user.char_id, payment_transaction_type: 'test', payment_transaction_id: 1,
				is_ok: true, client_application_id: 1, amount: 99, coins: '1', pay_type: 'TEST', created_at: Time.now, updated_at: Time.now}}
		end

		def buy_coins(user, ip)
			{'status' => 'ok'}
		end

		def buy(user, buy_data)
			{'status' => 'ok', 'coins' => '300'}
		end

		# Create a mock response
		def payment_history_redirect(user, ip)
			{'url' => 'http://de.lvh.me:3000/test/payment'}
		end
	end

	module OauthGuild
		def create_guild(guild, user)
			{'status' => 'ok', 'channel_id' => '1'}
		end

		def remove_guild(guild, user)
			{'status' => 'ok'}
		end

		def add_user(guild, user, new_member)
			{'status' => 'ok'}
		end

		def remove_user_from_guild(guild, user, member)
			{'status' => 'ok'}
		end
	end

	module OauthMessages

		def messages_index(user, mailbox)
			return {'status' => 'ok', 'messages' => {}}
		end

		def messages_show(user, message_id)
			return {'status' => 'ok', 'message' => {
				'created_at' => Time.now.to_i, 'to_id_names' => 'Recipient', 'subject' => 'Subject', 'message' => 'Lorem ipsum',
				'id' => message_id, 'from_user' => 'Sender'}}
		end

		def messages_destroy(user, message_id)
			return {'status' => 'ok'}
		end

		def messages_create(user, ip, message_type, to_char_id, to_unknown, subject, msg_text)
			return {'status' => 'ok'}
		end
	end

	module OauthFriends

		def friends_index(user)
			{'status' => 'ok', friends: {}}
		end

		def friends_show(user, friend_id)
			{'status' => 'ok', friend: {}}
		end

		def friends_destroy(user, friend_id)
			{'status' => 'ok'}
		end

		def friends_create(user, to_char_id, to_unknown)
			{'status' => 'ok'}
		end

		def friends_confirm(user, friend_id)
			{'status' => 'ok'}
		end
	end

	class TestToken
		attr_accessor :token
	end

	# Tokens for users
	# Auth codes to get user
	module MockOgiOauth
		@@mock_tokens = {}
		@@mock_auth_codes = {}

		def authorize(callback)
			'http://www.example.com/oauth'
		end

		def get_token(auth_code, callback)
			@@mock_auth_codes[auth_code]
		end

		def generate_auth_code(id)
			auth_code = SecureRandom.hex(8)
			token = TestToken.new
			token.token = SecureRandom.hex(8)
			@@mock_tokens[token] = id
			@@mock_auth_codes[auth_code] = token
			return auth_code
		end

		def get_user_id_by_token(token)
			@@mock_tokens[token].nil? ? 1 : @@mock_tokens[token]
		end
	end


	module MockOgiTokenApi
		def track_action(user, action, amount=1)
			return {'status' => 'ok'}
		end

		def short_messages_create(user, messages)
			return {'status' => 'ok'}
		end

		def short_messages_create_alliance(messages)
			return {'status' => 'ok'}
		end

		# to_char_language_map => {char_id => language, char_id2 => language}
		# msg_texts => {language => test, language => text}
		def messages_create_system_message(to_char_language_map, message_type, subjects, msg_texts)
			return {'status' => 'ok'}
		end

		def ranking_update_ogi(char_data)
			return {'status' => 'ok'}
		end

		def logout_page(user, ip)
			return {'status' => 'ok', 'url' => 'http://test.onlinegamesinfo.com'}
		end
	end
end

OnlinegamesInfoConnect::OgiOauth.instance.extend(OnlinegamesInfoConnect::MockOgiOauth)
OnlinegamesInfoConnect::OgiTokenApi.instance.extend(OnlinegamesInfoConnect::MockOgiTokenApi)

module OnlinegamesInfoConnect
	module OauthMessages

		def messages_index(user, mailbox)
			user.token.get("/api/messages", {params: {mailbox: mailbox}}).parsed
		end

		def messages_show(user, message_id)
			user.token.get("/api/messages/#{message_id}").parsed
		end

		def messages_destroy(user, message_id)
			user.token.delete("/api/messages/#{message_id}").parsed
		end

		def messages_create(user, ip, message_type, to_char_id, to_unknown, subject, msg_text)
			return user.token.post("/api/messages", {params: {ip: ip, message_type: message_type, subject: subject, message_text: msg_text,
																			  to_char_id: to_char_id, to_unknown: to_unknown}}).parsed
		end
	end
end

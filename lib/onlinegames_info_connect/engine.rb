module OnlinegamesInfoConnect
	class Engine < ::Rails::Engine
		require File.dirname(__FILE__)+'/authenticated_system'
		require File.dirname(__FILE__)+'/oauth_friends'
		require File.dirname(__FILE__)+'/oauth_guild'
		require File.dirname(__FILE__)+'/oauth_medals'
		require File.dirname(__FILE__)+'/oauth_messages'
		require File.dirname(__FILE__)+'/oauth_premium'
		require File.dirname(__FILE__)+'/oauth_users'
		require File.dirname(__FILE__)+'/ogi_oauth'
		require File.dirname(__FILE__)+'/ogi_token_api'
		require File.dirname(__FILE__)+'/test_helper'
	end
end

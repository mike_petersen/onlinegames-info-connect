module OnlinegamesInfoConnect
	module OauthPremium
		def add_free_coins(user, amount, reason)
			return user.token.post("/api/premium/#{user.ogi_uid}", {body: {amount: amount, reason: reason}}).parsed
		end

		def balance(user)
			return user.token.get("/api/premium/#{user.ogi_uid}/balance").parsed
		end

		def buy_coins(user, ip)
			return user.token.get("/api/premium/#{user.ogi_uid}/payment_redirect", {params: {ip: ip}}).parsed
		end

		def buy(user, buy_data)
			begin
				result = user.token.post("/api/premium/#{user.ogi_uid}/buy", {params: buy_data}).parsed
			rescue
				return {'flash_msg' => I18n.t("Error")}
			end

			if result.nil? or result['status'].nil? or not "ok".eql?(result['status'])
				result ||= {}
				result['flash_msg'] = I18n.t("Error")
				result['flash_msg'] = I18n.t("portal_error_#{result['error_code']}") if not result.nil? and not result['error_code'].nil? and not result['error_code'].empty?
			end

			return result
		end

		def buy_premium(user, buy_data)
			result = buy(user, buy_data)
			return result['flash_msg'] if result['status'].nil? or not "ok".eql?(result['status'])
			user.coins = result['coins'].to_i

			return true
		end

		def payment_history_redirect(user, ip)
			return user.token.get("/api/premium/#{user.ogi_uid}/redirect", {params: {ip: ip}}).parsed
		end
	end
end

module OnlinegamesInfoConnect
	module OauthMedals
		def add_medal(user, medal_id, level)
			return user.token.post("/api/medals/#{user.id}", {body: {medal_id: medal_id, level: level}}).parsed
		end

		def get_medals(user)
			return user.token.get("/api/medals/#{user.id}").parsed
		end
	end
end

module OnlinegamesInfoConnect
	module OauthFriends

		def friends_index(user)
			user.token.get("/api/friends").parsed
		end

		def friends_show(user, friend_id)
			user.token.get("/api/friends/#{friend_id}").parsed
		end

		def friends_destroy(user, friend_id)
			user.token.delete("/api/friends/#{friend_id}").parsed
		end

		def friends_create(user, to_char_id, to_unknown)
			return user.token.post("/api/friends", {params: {to_char_id: to_char_id, to_unknown: to_unknown}}).parsed
		end

		def friends_confirm(user, friend_id)
			return user.token.post("/api/friends/#{friend_id}/confirm").parsed
		end
	end
end

module OnlinegamesInfoConnect
	module OauthUsers
		def me(token)
			return token.get('/api/users/me.json').parsed
		end

		def set_user_name(user, char_name)
			return user.token.post("/api/users/#{user.id}/set_name", {params: {char_name: char_name}}).parsed
		end
	end
end

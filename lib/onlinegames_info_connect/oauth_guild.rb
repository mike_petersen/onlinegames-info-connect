module OnlinegamesInfoConnect
	module OauthGuild
		def create_guild(guild, user)
			return user.token.post("/api/guilds", {body: {name: guild.name, tag: guild.tag, user_id: user.ogi_uid, char_id: user.char_id}}).parsed
		end

		def remove_guild(guild, user)
			return user.token.post("/api/guilds/remove", {body: {guild_id: guild.ogi_alliance_id}}).parsed
		end

		def add_user(guild, user, new_member)
			return user.token.post("/api/guilds/add_user", {body: {guild_id: guild.ogi_alliance_id, user_id: new_member.ogi_uid, char_id: new_member.char_id}}).parsed
		end

		def remove_user_from_guild(guild, user, member)
			return user.token.post("/api/guilds/remove_user", {body: {guild_id: guild.ogi_alliance_id, user_id: member.ogi_uid, char_id: member.char_id}}).parsed
		end
	end
end

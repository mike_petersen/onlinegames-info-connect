require 'oauth2'

module OnlinegamesInfoConnect
	class OgiOauth
		include Singleton

		# Include Additional classes
		include OauthUsers
		include OauthMedals
		include OauthPremium
		include OauthGuild
		include OauthMessages
		include OauthFriends

		def get_client
			site = OGI_SETTINGS[:site].gsub('MARKET', I18n.locale.to_s)
			@client ||= OAuth2::Client.new(OGI_SETTINGS[:key], OGI_SETTINGS[:secret], :site => site)
		end

		def authorize(callback)
			get_client.auth_code.authorize_url(:redirect_uri => get_redirect_url(callback))
		end

		def get_token(auth_code, callback)
			return get_client.auth_code.get_token(auth_code, :redirect_uri => get_redirect_url(callback))
		end

		private

		def get_redirect_url(callback)
			return OGI_SETTINGS[:redirect_url].gsub('MARKET', I18n.locale.to_s) unless OGI_SETTINGS[:redirect_url].nil?
			return callback
		end
	end
end

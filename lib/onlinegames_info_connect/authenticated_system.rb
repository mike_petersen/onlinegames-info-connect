module OnlinegamesInfoConnect
	module AuthenticatedSystem
		mattr_accessor :my_user, :roles

		protected

		def current_user
			return false if session[:user_id].nil?
			begin
				@my_user = User.where(:id => session[:user_id]).first if @my_user.nil?
				return @my_user
			rescue
				return false
			end
		end

		def logged_in?
			(current_user.nil? or current_user.class != User) ? false : true
		end

		def authorized?

		end

		def require_no_user
			if logged_in? then
				redirect_to game_path
				return false
			end

			return true
		end

		def require_user
			if not logged_in? then
				store_location

				respond_to do |format|
					format.html { redirect_to login_path }
					format.json { render :json => {status: 'not_authenticated'} }
				end

				return false
			end

			return init_user
		end

		def has_permission(role_type)
			return false if not logged_in?
			return current_user.has_right(role_type)
		end

		def check_user_role(role_type)
			@roles = {} if @roles.nil?
			if @roles[role_type].nil?
				role = UserRole.where(:user_id => current_user.id, :role => role_type).first
				@roles[role_type] = role.nil? ? false : true
			end

			return @roles[role_type]
		end

		def check_permission(role_type)
			if not has_permission(role_type) then
				redirect_to game_path
				return false
			end

			return true
		end


		def check_user
			if not logged_in? then
				return true
			end

			return init_user
		end

		def store_location
			session[:return_to] = request.fullpath
		end

		def redirect_back_or_default(default)
			redirect_to(session[:return_to] || default)
			session[:return_to] = nil
		end
	end
end

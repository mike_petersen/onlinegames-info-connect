module OnlinegamesInfoConnect
	class OgiTokenApi
		include Singleton

		def track_action(user, action, amount=1)
			make_request("/api/tracking/track_action", {track_action: action, amount: amount, user_id: user.ogi_uid, char_id: user.char_id})
		end

		def short_messages_create(user, messages)
			make_request("/api/system_messages/create_short_message", {messages: messages.to_json, user_id: user.ogi_uid, char_id: user.char_id})
		end

		def short_messages_create_alliance(messages)
			make_request("/api/system_messages/create_short_message", {messages: messages.to_json})
		end

		# to_char_language_map => {char_id => language, char_id2 => language}
		# msg_texts => {language => test, language => text}
		def messages_create_system_message(to_char_language_map, message_type, subjects, msg_texts)
			make_request("/api/system_messages/create", {to_char_language_map: to_char_language_map, message_type: message_type, subjects: subjects, message_texts: msg_texts})
		end

		def ranking_update_ogi(char_data)
			make_request("/api/ranking", {char_data: ActiveSupport::JSON.encode(char_data)})
		end

		def create_user(ip, login, email, password, campaign, language, ref_id)
			make_request("/api/users/sign_up",
											  {params: {ip: ip, password: password, login: login, email: email, campaign: campaign, language: language, ref_id: ref_id}})
		end

		def logout_page(user, ip)
			make_request("/api/users/#{user.id}/logout_page", {ip: ip})
		end

		private

		def make_request(path, data)
			site = OGI_SETTINGS[:site].gsub('MARKET', I18n.locale.to_s)
			response = Faraday.post do |req|
				req.url site+path
				req.headers['Authorization'] = "Token token=\"#{OGI_SETTINGS[:secret]}\""
				req.body = data
			end

			return JSON.parse response.body
		end
	end
end

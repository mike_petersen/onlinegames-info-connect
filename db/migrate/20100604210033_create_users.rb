class OgiConnect < ActiveRecord::Migration
	def self.up
		create_table :users do |t|
			t.integer :ogi_uid, :null => false
			t.integer :char_id, :null => true
			t.string :name
			t.string :email
			t.string :crypted_password, :null => true
			t.string :password_salt, :null => true
			t.string :persistence_token
			t.string :single_access_token
			t.string :perishable_token
			t.string :language
			t.date :birthday
			t.string :gender
			t.datetime :premium_time, :null => false
			t.string :user_status
			t.integer :alliance_id
			t.integer :ref_id
			t.string :campaign
			t.integer :coins
			t.boolean :is_bot, :default => false
			t.integer :daily_login_count, :default => 0
			t.datetime :vacation

			t.integer :login_count, :null => false, :default => 0
			t.integer :failed_login_count, :null => false, :default => 0
			t.datetime :last_request_at
			t.datetime :current_login_at
			t.datetime :last_login_at
			t.string :current_login_ip
			t.string :last_login_ip

			t.column "rnk", :integer, :default => 0, :null => false
			t.column "rnkgespkt", :integer, :default => 0, :null => false
			t.column "rnkmilpkt", :integer, :default => 0, :null => false
			t.column "rnkforpkt", :integer, :default => 0, :null => false
			t.column "rnkspipkt", :integer, :default => 0, :null => false
			t.column "rnkgebpkt", :integer, :default => 0, :null => false
			t.column "rnk_beruf", :integer, :default => 0, :null => false
			t.column "member", :integer, :default => 0, :null => false
			t.column "rnk_werbepkt", :integer, :default => 0, :null => false
			t.column "rnk_geworbene", :integer, :default => 0, :null => false
			t.column "spio_missionen", :integer, :default => 0, :null => false
			t.column "sabo_missionen", :integer, :default => 0, :null => false
			t.column "gegenspio_missionen", :integer, :default => 0, :null => false
			t.column "spio_missionen_success", :integer, :default => 0, :null => false
			t.column "sabo_missionen_success", :integer, :default => 0, :null => false
			t.column "gegenspio_missionen_success", :integer, :default => 0, :null => false
			t.column "spio_missionen_failure", :integer, :default => 0, :null => false
			t.column "sabo_missionen_failure", :integer, :default => 0, :null => false
			t.column "gegenspio_missionen_failure", :integer, :default => 0, :null => false
			t.column "spys_captured", :integer, :default => 0, :null => false
			t.column "spys_killed", :integer, :default => 0, :null => false
			t.column "spys_randsom_got", :integer, :default => 0, :null => false
			t.column "spys_randsom_payed", :integer, :default => 0, :null => false

			t.column :oauth_token, :string
			t.column :info_race_id, :integer

			t.timestamps
		end

		add_index :users, :name
		add_index :users, :persistence_token
		add_index :users, :rnk

		create_table :languages do |t|
			t.string :short
			t.string :name
			t.timestamps
		end

		create_table :user_roles do |t|
			t.integer :user_id
			t.string :role

			t.timestamps
		end
	end

	def self.down
		drop_table :users
		drop_table :languages
		drop_table :user_roles
	end
end

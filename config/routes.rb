Rails.application.routes.draw do
	get "authorize" => "session#new"
	get "callback" => "session#callback"

	get "login" => "session#index"
	get "sign_up" => "session#index"
	match "logout" => "session#destroy", via: [:get, :post, :delete]
	post 'users' => "users#create", :as => :user

	namespace :control_center do
		resources :languages, :user_roles
		resources :users do
			collection do
				get :check_user
				get :login
			end
		end
	end

	namespace :game_config do
		root :to => "dashboard#index"
	end
end
